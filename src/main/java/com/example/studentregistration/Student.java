package com.example.studentregistration;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.text.MessageFormat;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
public class Student implements Serializable {
    private UUID id;
    private String firstName;
    private String lastName;
    private int age;

    public Student(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return MessageFormat.format("Student {0} {1} {2} years old (id: {3})",
                firstName, lastName, age, id);
    }
}
