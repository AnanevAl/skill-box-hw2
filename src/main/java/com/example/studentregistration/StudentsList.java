package com.example.studentregistration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Component
public class StudentsList {
    private List<Student> students = new ArrayList<>();
    private final ApplicationEventPublisher eventPublisher;

    @Autowired
    public StudentsList(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }


    public void add(Student student){
        students.add(student);
        eventPublisher.publishEvent(new StudentAddedEvent(this, student));
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (Student student:students){
            stringBuilder.append(MessageFormat.format("Student: id: {0}; First name: {1}; Last name: {2}, Age: {3}",
                    student.getId(),
                    student.getFirstName(),
                    student.getLastName(),
                    student.getAge())).append(System.lineSeparator());
        }
        return stringBuilder.toString();
    }

    public void delete(UUID id) {
        students.removeIf(student -> student.getId().equals(id));
        eventPublisher.publishEvent(new StudentDeletedEvent(this, id));
    }

    public void clear() {
        students.clear();
    }
}
