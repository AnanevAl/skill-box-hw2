package com.example.studentregistration.mock;

import org.springframework.context.annotation.*;

@ComponentScan("com.example")
@Configuration
@PropertySource("classpath:application-mock.properties")
@Profile("demo")
public class MockDataAppConfig {
    @Bean
    public MockDataReader mockDataReader(){
        return new MockDataReader();
    }
}
