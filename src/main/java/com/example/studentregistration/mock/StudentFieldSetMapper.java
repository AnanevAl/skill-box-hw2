package com.example.studentregistration.mock;

import com.example.studentregistration.Student;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindException;

import java.util.UUID;

@Component
public class StudentFieldSetMapper implements FieldSetMapper<Student> {
    @Override
    public Student mapFieldSet(FieldSet fieldSet) throws BindException {
        Student student = new Student();
        student.setId(UUID.fromString(fieldSet.readString(0)));
        student.setFirstName(fieldSet.readString(1));
        student.setLastName(fieldSet.readString(2));
        student.setAge(fieldSet.readInt(3));
        return student;
    }
}
