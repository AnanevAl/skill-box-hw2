package com.example.studentregistration.mock;

import com.example.studentregistration.Student;
import com.example.studentregistration.StudentsList;
import jakarta.annotation.PostConstruct;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;

public class MockDataReader {
    @Value("${app.data.source}")
    private String SOURCE_DATA;
    private StudentsList studentsList;
    private FlatFileItemReader<Student> itemReader = new FlatFileItemReader<>();
    private DefaultLineMapper<Student> lineMapper = new DefaultLineMapper<>();
    private StudentFieldSetMapper studentFieldSetMapper;

    @Autowired
    public void setStudentFieldSetMapper(StudentFieldSetMapper studentFieldSetMapper) {
        this.studentFieldSetMapper = studentFieldSetMapper;
    }
    @Autowired
    public void setStudentsList(StudentsList studentsList) {
        this.studentsList = studentsList;
    }

    @PostConstruct
    private void init(){
        this.itemReader.setResource(new FileSystemResource(SOURCE_DATA));
        this.lineMapper.setLineTokenizer(new DelimitedLineTokenizer());
        this.lineMapper.setFieldSetMapper(studentFieldSetMapper);
        this.itemReader.setLineMapper(lineMapper);
        this.itemReader.open(new ExecutionContext());
        readMockData();
    }
    private Student read() throws Exception {
        return itemReader.read();
    }

    private void readMockData(){
        try {
            for (;;){
                Student student = read();
                if (student == null) break;
                studentsList.add(student);
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
