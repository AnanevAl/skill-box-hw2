package com.example.studentregistration;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class StudentsListListener {
    @EventListener
    public void onStudentAdded(StudentAddedEvent event){
        System.out.println("Student added: " + event.getStudent().toString());
    }

    @EventListener
    public void onStudentDeleted(StudentDeletedEvent event){
        System.out.println("Student deleted: " + event.getId());
    }


}
