package com.example.studentregistration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.UUID;

@ShellComponent
public class StudentRegister {
    private final StudentsList studentsList;

    @Autowired
    public StudentRegister(StudentsList studentsList) {
        this.studentsList = studentsList;
    }


    @ShellMethod(key = {"a", "add"})
    public void addStudent(@ShellOption(value = {"fn", "firstName"}) String firsName,
                           @ShellOption(value = {"ln", "lastName"}) String lastName,
                           @ShellOption(value = {"a", "age"}) int age){
        Student student = new Student(firsName, lastName, age);
        studentsList.add(student);
    }

    @ShellMethod(key = {"d", "delete"})
    public void deleteStudent(@ShellOption(value = "id") UUID id){
        studentsList.delete(id);
    }

    @ShellMethod(key = {"ls", "list"})
    public String listStudent(){
        return studentsList.toString();
    }

    @ShellMethod(key = {"cl", "clear"})
    public void clearStudents(){
        studentsList.clear();
    }


}
