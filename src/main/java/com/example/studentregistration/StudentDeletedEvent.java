package com.example.studentregistration;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

import java.util.UUID;

@Getter
public class StudentDeletedEvent extends ApplicationEvent {
    private final UUID id;
    public StudentDeletedEvent(Object source, UUID id) {
        super(source);
        this.id = id;
    }
}
