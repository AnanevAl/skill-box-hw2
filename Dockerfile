FROM openjdk:17-oracle
WORKDIR /app
COPY build/libs/student-registration-0.0.1.jar app.jar
COPY build/resources/main/mock-data.csv /app/src/main/resources/mock-data.csv
CMD ["java", "-jar", "app.jar"]