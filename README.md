# Консольно приложение регистрации студентов
Приложение позволяет:

* Добавлять новых студентов
* Удаляет студента по id
* Очищает список студентов

# Использование

### Возможные команды
* a,add : Добавление студента

    Параметры:
  * fn, firstName - Имя
  * ls, lastName - Фамилия
  * a, age - Возраст
  
  Пример:
    ```
    a --fn Billy --ln Trousdell --a 25
    ```
* d,delete : Удаление студента
  Параметры:
    * id - Идентификатор студента

* ls, list : Вывод списка студентов
* cl, clear : Очищает список студентов

# Установка и запуск из Docker
Получение образа:
```
docker pull aananyevkz/student-registration
```
Запуск контейнера по умолчанию:
```
 docker run -it --rm aananyevkz/student-registration
```
Запуск контейнера c демонстрационными данными:
```
 docker run -e "SPRING_PROFILES_ACTIVE=demo" -it --rm aananyevkz/student-registration
```